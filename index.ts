import { Prisma, PrismaClient } from '@prisma/client'
import { performance } from 'perf_hooks';
import { original_args } from './original-query';
import { or_removed } from './or_removed';
import { unnecessary_where_removed } from './unnecessary_where_removed';
import { unnecessary_where_removed2 } from './unnecessary_where_removed2';

const prisma = new PrismaClient({
    log: [{
        emit: 'event',
        level: 'query',
    }]
});

prisma.$on('query', e => {
    console.log("Query: " + e.query);
    console.log("params: " + e.params);
    console.log("Duration: " + e.duration + "ms");
});

async function main() {
    const startTime = performance.now();
    // const query = Prisma.validator<Prisma.operation_alphaFindManyArgs>()({});
    // console.log(query);
    const address = prisma.operation_alpha.findMany(or_removed);
    const a = await address;
    const endTime = performance.now();
    console.log(`Total Time: ${endTime - startTime} ms`);
    console.log(a);
}

main()
    .catch(e => {
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    });
