export const original_args = {
    where: {
        OR: [
            {
                operation: {
                    hash: {
                        gt: "onwru99FepqyAgypPMjXaFbeDEGM2cD7frRnrHjpKdcVQWCxL4d"
                    }
                },
                block: {
                    timestamp: {},
                    level: {
                        equals: 1282790
                    }
                },
                autoid: {},
                id: {},
                operation_sender_and_receiver: {
                    addresses_addressesTooperation_sender_and_receiver_sender_id: {
                        address: {}
                    }
                }
            },
            {
                block: {
                    timestamp: {},
                    level: {
                        lt: 1282790
                    }
                },
                autoid: {},
                id: {},
                operation_sender_and_receiver: {
                    addresses_addressesTooperation_sender_and_receiver_sender_id: {
                        address: {}
                    }
                }
            }
        ],
        operation: {
            hash: {
                gt: "onwru99FepqyAgypPMjXaFbeDEGM2cD7frRnrHjpKdcVQWCxL4d"
            }
        },
        block: {
            timestamp: {
                gte: "2021-01-01T00:00:00.000Z",
                lte: "2021-01-01T00:02:00.000Z"
            },
            level: {
                lt: 1282790
            },
            block_hash: {}
        },
        autoid: {},
        id: {},
        operation_sender_and_receiver: {
            addresses_addressesTooperation_sender_and_receiver_sender_id: {
                address: {}
            }
        }
    },
    select: {
        id: true,
        autoid: true,
        operation: {
            select: {
                hash: true
            }
        },
        block: {
            select: {
                timestamp: true,
                level: true,
                block_hash: {
                    select: {
                        hash: true
                    }
                }
            }
        },
        operation_kind: true,
        operation_sender_and_receiver: {
            select: {
                addresses_addressesTooperation_sender_and_receiver_sender_id: {
                    select: {
                        address: true
                    }
                }
            }
        },
        manager_numbers: {
            select: {
                gas_limit: true,
                counter: true,
                storage_limit: true
            }
        },
        endorsement: {
            select: {
                slots: true,
                addresses: {
                    select: {
                        address: true
                    }
                }
            }
        },
        delegation: {
            select: {
                consumed_milligas: true,
                addresses_addressesTodelegation_pkh_id: {
                    select: {
                        address: true
                    }
                },
                fee: true
            }
        },
        reveal: {
            select: {
                consumed_milligas: true,
                pk: true,
                fee: true
            }
        },
        origination: {
            select: {
                consumed_milligas: true,
                addresses_addressesToorigination_k_id: {
                    select: {
                        address: true
                    }
                },
                fee: true
            }
        },
        tx: {
            select: {
                consumed_milligas: true,
                parameters: true,
                entrypoint: true,
                amount: true,
                addresses_addressesTotx_destination_id: {
                    select: {
                        address: true
                    }
                },
                fee: true
            }
        }
    },
    take: 4,
    orderBy: [
        {
            block: {
                level: "desc" as const
            }
        },
        {
            operation: {
                hash: "asc" as const
            }
        },
        {
            id: "asc" as const
        }
    ]
};