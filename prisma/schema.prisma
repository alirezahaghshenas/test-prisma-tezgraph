generator client {
  provider = "prisma-client-js"
  previewFeatures = ["orderByRelation"]
  binaryTargets   = ["native", "linux-musl"]
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model activation {
  operation_id    BigInt          @id
  pkh_id          BigInt
  activation_code String
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses       addresses       @relation(fields: [pkh_id], references: [address_id])
}

model addresses {
  address                                                                            String                          @id @db.Char(36)
  address_id                                                                         BigInt?                         @unique
  activation                                                                         activation[]
  balance_updates_block                                                              balance_updates_block[]
  balance_updates_op                                                                 balance_updates_op[]
  bigmap_addressesTobigmap_receiver_id                                               bigmap[]                        @relation("addressesTobigmap_receiver_id")
  bigmap_addressesTobigmap_sender_id                                                 bigmap[]                        @relation("addressesTobigmap_sender_id")
  block_alpha                                                                        block_alpha[]
  contract_addressesTocontract_address_id                                            contract[]                      @relation("addressesTocontract_address_id")
  contract_addressesTocontract_delegate_id                                           contract[]                      @relation("addressesTocontract_delegate_id")
  contract_addressesTocontract_mgr_id                                                contract[]                      @relation("addressesTocontract_mgr_id")
  contract_addressesTocontract_preorig_id                                            contract[]                      @relation("addressesTocontract_preorig_id")
  contract_balance                                                                   contract_balance[]
  deactivated                                                                        deactivated[]
  delegated_contract_addressesTodelegated_contract_delegate_id                       delegated_contract[]            @relation("addressesTodelegated_contract_delegate_id")
  delegated_contract_addressesTodelegated_contract_delegator_id                      delegated_contract[]            @relation("addressesTodelegated_contract_delegator_id")
  delegation_addressesTodelegation_pkh_id                                            delegation[]                    @relation("addressesTodelegation_pkh_id")
  delegation_addressesTodelegation_source_id                                         delegation[]                    @relation("addressesTodelegation_source_id")
  endorsement                                                                        endorsement[]
  operation_sender_and_receiver_addressesTooperation_sender_and_receiver_receiver_id operation_sender_and_receiver[] @relation("addressesTooperation_sender_and_receiver_receiver_id")
  operation_sender_and_receiver_addressesTooperation_sender_and_receiver_sender_id   operation_sender_and_receiver[] @relation("addressesTooperation_sender_and_receiver_sender_id")
  origination_addressesToorigination_k_id                                            origination[]                   @relation("addressesToorigination_k_id")
  origination_addressesToorigination_source_id                                       origination[]                   @relation("addressesToorigination_source_id")
  proposal                                                                           proposal[]
  reveal                                                                             reveal[]
  tx_addressesTotx_destination_id                                                    tx[]                            @relation("addressesTotx_destination_id")
  tx_addressesTotx_source_id                                                         tx[]                            @relation("addressesTotx_source_id")

  @@index([address_id], name: "addresses_autoid")
}

model balance_updates_block {
  block_hash_id       Int
  balance_kind        Int       @db.SmallInt
  contract_address_id BigInt
  cycle               Int?
  diff                BigInt
  id                  Int
  block               block     @relation(fields: [block_hash_id], references: [hash_id])
  addresses           addresses @relation(fields: [contract_address_id], references: [address_id])

  @@id([block_hash_id, id])
  @@index([block_hash_id], name: "balance_block")
  @@index([balance_kind], name: "balance_cat")
  @@index([cycle], name: "balance_cycle")
  @@index([contract_address_id], name: "balance_k")
}

model balance_updates_op {
  operation_id        BigInt
  balance_kind        Int             @db.SmallInt
  contract_address_id BigInt
  cycle               Int?
  diff                BigInt
  id                  Int
  addresses           addresses       @relation(fields: [contract_address_id], references: [address_id])
  operation_alpha     operation_alpha @relation(fields: [operation_id], references: [autoid])

  @@id([operation_id, id])
  @@index([operation_id], name: "balance_operation_id")
}

model ballot {
  operation_id    BigInt                    @id
  source_id       BigInt
  period          Int
  proposal_id     BigInt
  ballot          Unsupported("one_ballot")
  operation_alpha operation_alpha           @relation(fields: [operation_id], references: [autoid])
  proposals       proposals                 @relation(fields: [proposal_id], references: [proposal_id])

  @@index([source_id], name: "ballot_source")
}

model bigmap {
  id                                      BigInt?
  key                                     Json?
  key_hash                                String?   @db.Char(54)
  key_type                                Json?
  value                                   Json?
  value_type                              Json?
  block_hash_id                           Int
  block_level                             Int
  sender_id                               BigInt
  receiver_id                             BigInt
  name                                    String?
  i                                       BigInt
  block                                   block     @relation(fields: [block_hash_id, block_level], references: [hash_id, level])
  addresses_addressesTobigmap_receiver_id addresses @relation("addressesTobigmap_receiver_id", fields: [receiver_id], references: [address_id])
  addresses_addressesTobigmap_sender_id   addresses @relation("addressesTobigmap_sender_id", fields: [sender_id], references: [address_id])

  @@id([block_hash_id, i])
  @@index([block_hash_id], name: "bigmap_block_hash")
  @@index([block_level], name: "bigmap_block_level")
  @@index([id], name: "bigmap_id")
  @@index([key], name: "bigmap_key")
  @@index([key_hash], name: "bigmap_key_hash")
  @@index([name], name: "bigmap_name")
  @@index([receiver_id], name: "bigmap_receiver")
  @@index([sender_id], name: "bigmap_sender")
}

model block {
  hash_id               Int                     @unique
  level                 Int
  proto                 Int
  predecessor_id        Int
  timestamp             DateTime                @db.Timestamp(6)
  validation_passes     Int                     @db.SmallInt
  merkle_root           String                  @db.Char(53)
  fitness               String                  @db.VarChar(64)
  context_hash          String                  @db.Char(52)
  rejected              Boolean                 @default(false)
  indexing_depth        Int                     @default(0) @db.SmallInt
  block_hash            block_hash              @relation(fields: [hash_id], references: [hash_id])
  block                 block                   @relation("blockToblock_predecessor_id", fields: [predecessor_id], references: [hash_id])
  balance_updates_block balance_updates_block[]
  bigmap                bigmap[]
  other_block           block[]                 @relation("blockToblock_predecessor_id")
  block_alpha           block_alpha?
  contract              contract[]
  contract_balance      contract_balance[]
  deactivated           deactivated[]
  operation             operation[]
  operation_alpha       operation_alpha[]

  @@id([hash_id, level])
  @@index([hash_id], name: "block_hash_id")
  @@index([level], name: "block_level")
  @@index([rejected], name: "block_rejected")
}

model block_alpha {
  hash_id                Int       @id
  baker_id               BigInt
  level_position         Int
  cycle                  Int
  cycle_position         Int
  voting_period          Json
  voting_period_position Int
  voting_period_kind     Int       @db.SmallInt
  consumed_milligas      Decimal   @db.Decimal
  addresses              addresses @relation(fields: [baker_id], references: [address_id])
  block                  block     @relation(fields: [hash_id], references: [hash_id])

  @@index([baker_id], name: "block_alpha_baker")
  @@index([cycle], name: "block_alpha_cycle")
  @@index([cycle_position], name: "block_alpha_cycle_position")
  @@index([level_position], name: "block_alpha_level_position")
}

model block_hash {
  hash    String @id @db.Char(51)
  hash_id Int    @unique
  block   block?

  @@index([hash_id], name: "block_hash_hash_id")
}

model chain {
  hash String @id @db.Char(15)
}

model contract {
  address_id                                BigInt
  block_hash_id                             Int
  mgr_id                                    BigInt?
  delegate_id                               BigInt?
  spendable                                 Boolean?
  delegatable                               Boolean?
  credit                                    BigInt?
  preorig_id                                BigInt?
  script                                    Json?
  block_level                               Int
  addresses_addressesTocontract_address_id  addresses  @relation("addressesTocontract_address_id", fields: [address_id], references: [address_id])
  block                                     block      @relation(fields: [block_hash_id], references: [hash_id])
  addresses_addressesTocontract_delegate_id addresses? @relation("addressesTocontract_delegate_id", fields: [delegate_id], references: [address_id])
  addresses_addressesTocontract_mgr_id      addresses? @relation("addressesTocontract_mgr_id", fields: [mgr_id], references: [address_id])
  addresses_addressesTocontract_preorig_id  addresses? @relation("addressesTocontract_preorig_id", fields: [preorig_id], references: [address_id])

  @@id([address_id, block_hash_id])
  @@index([address_id], name: "contract_address")
  @@index([block_hash_id], name: "contract_block")
  @@index([block_level], name: "contract_block_level")
  @@index([delegate_id], name: "contract_delegate")
  @@index([mgr_id], name: "contract_mgr")
  @@index([preorig_id], name: "contract_preorig")
}

model contract_balance {
  address_id    BigInt
  block_hash_id Int
  balance       BigInt?
  block_level   Int
  addresses     addresses @relation(fields: [address_id], references: [address_id])
  block         block     @relation(fields: [block_hash_id, block_level], references: [hash_id, level])

  @@id([address_id, block_hash_id])
  @@index([address_id], name: "contract_balance_address")
  @@index([address_id, block_level], name: "contract_balance_address_block_level")
  @@index([block_hash_id], name: "contract_balance_block_hash")
  @@index([block_level], name: "contract_balance_block_level")
}

model deactivated {
  pkh_id        BigInt
  block_hash_id Int
  block         block     @relation(fields: [block_hash_id], references: [hash_id])
  addresses     addresses @relation(fields: [pkh_id], references: [address_id])

  @@id([pkh_id, block_hash_id])
  @@index([block_hash_id], name: "deactivated_block_hash")
  @@index([pkh_id], name: "deactivated_pkh")
}

model delegated_contract {
  delegate_id                                          BigInt
  delegator_id                                         BigInt
  cycle                                                Int
  level                                                Int
  snapshot                                             snapshot  @relation(fields: [cycle, level], references: [cycle, level])
  addresses_addressesTodelegated_contract_delegate_id  addresses @relation("addressesTodelegated_contract_delegate_id", fields: [delegate_id], references: [address_id])
  addresses_addressesTodelegated_contract_delegator_id addresses @relation("addressesTodelegated_contract_delegator_id", fields: [delegator_id], references: [address_id])

  @@id([delegate_id, delegator_id, cycle, level])
  @@index([cycle], name: "delegated_contract_cycle")
  @@index([delegate_id], name: "delegated_contract_delegate")
  @@index([delegator_id], name: "delegated_contract_delegator")
  @@index([level], name: "delegated_contract_level")
}

model delegation {
  operation_id                              BigInt          @id
  source_id                                 BigInt
  pkh_id                                    BigInt?
  consumed_milligas                         Decimal?        @db.Decimal
  fee                                       BigInt
  nonce                                     Int?
  operation_alpha                           operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses_addressesTodelegation_pkh_id    addresses?      @relation("addressesTodelegation_pkh_id", fields: [pkh_id], references: [address_id])
  addresses_addressesTodelegation_source_id addresses       @relation("addressesTodelegation_source_id", fields: [source_id], references: [address_id])

  @@index([operation_id], name: "delegation_operation_hash")
  @@index([pkh_id], name: "delegation_pkh")
  @@index([source_id], name: "delegation_source")
}

model double_baking_evidence {
  operation_id    BigInt          @id
  bh1             Json
  bh2             Json
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
}

model double_endorsement_evidence {
  operation_id    BigInt          @id
  op1             Json
  op2             Json
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
}

model endorsement {
  operation_id    BigInt          @id
  level           Int?
  delegate_id     BigInt?
  slots           Int[]           @db.SmallInt
  addresses       addresses?      @relation(fields: [delegate_id], references: [address_id])
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])

  @@index([delegate_id], name: "endorsement_delegate_id")
}

model manager_numbers {
  operation_id    BigInt          @id
  counter         Decimal?        @db.Decimal
  gas_limit       Decimal?        @db.Decimal
  storage_limit   Decimal?        @db.Decimal
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
}

model operation {
  hash            String            @db.Char(51)
  block_hash_id   Int
  hash_id         BigInt            @unique
  block           block             @relation(fields: [block_hash_id], references: [hash_id])
  operation_alpha operation_alpha[]

  @@id([hash_id, block_hash_id])
  @@index([block_hash_id], name: "operation_block")
  @@index([hash], name: "operation_hash")
  @@index([hash_id], name: "operation_hash_id")
}

model operation_alpha {
  block_hash_id                 Int
  hash_id                       BigInt
  id                            Int                            @db.SmallInt
  operation_kind                Int                            @db.SmallInt
  internal                      Int                            @db.SmallInt
  autoid                        BigInt                         @unique
  block                         block                          @relation(fields: [block_hash_id], references: [hash_id])
  operation                     operation                      @relation(fields: [hash_id], references: [hash_id])
  activation                    activation?
  balance_updates_op            balance_updates_op[]
  ballot                        ballot?
  delegation                    delegation?
  double_baking_evidence        double_baking_evidence?
  double_endorsement_evidence   double_endorsement_evidence?
  endorsement                   endorsement?
  manager_numbers               manager_numbers?
  operation_sender_and_receiver operation_sender_and_receiver?
  origination                   origination?
  proposal                      proposal?
  reveal                        reveal?
  seed_nonce_revelation         seed_nonce_revelation?
  tx                            tx?

  @@id([hash_id, id, internal, block_hash_id])
  @@index([autoid], name: "operation_alpha_autoid")
  @@index([block_hash_id], name: "operation_alpha_block_hash")
  @@index([hash_id], name: "operation_alpha_hash")
  @@index([operation_kind], name: "operation_alpha_kind")
}

model operation_sender_and_receiver {
  operation_id                                                   BigInt          @id
  sender_id                                                      BigInt
  receiver_id                                                    BigInt?
  operation_alpha                                                operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses_addressesTooperation_sender_and_receiver_receiver_id addresses?      @relation("addressesTooperation_sender_and_receiver_receiver_id", fields: [receiver_id], references: [address_id])
  addresses_addressesTooperation_sender_and_receiver_sender_id   addresses       @relation("addressesTooperation_sender_and_receiver_sender_id", fields: [sender_id], references: [address_id])

  @@index([receiver_id], name: "operation_sender_and_receiver_receiver")
  @@index([sender_id], name: "operation_sender_and_receiver_sender")
}

model origination {
  operation_id                               BigInt          @id
  source_id                                  BigInt
  k_id                                       BigInt
  consumed_milligas                          Decimal         @db.Decimal
  storage_size                               Decimal         @db.Decimal
  paid_storage_size_diff                     Decimal         @db.Decimal
  fee                                        BigInt
  nonce                                      Int?
  addresses_addressesToorigination_k_id      addresses       @relation("addressesToorigination_k_id", fields: [k_id], references: [address_id])
  operation_alpha                            operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses_addressesToorigination_source_id addresses       @relation("addressesToorigination_source_id", fields: [source_id], references: [address_id])

  @@index([k_id], name: "origination_k")
  @@index([source_id], name: "origination_source")
}

model proposal {
  operation_id    BigInt          @id
  source_id       BigInt
  period          Int
  proposal_id     BigInt
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
  proposals       proposals       @relation(fields: [proposal_id], references: [proposal_id])
  addresses       addresses       @relation(fields: [source_id], references: [address_id])

  @@index([operation_id], name: "proposal_operation")
  @@index([period], name: "proposal_period")
  @@index([proposal_id], name: "proposal_proposal")
  @@index([source_id], name: "proposal_source")
}

model proposals {
  proposal                     String     @id @db.Char(51)
  proposal_id                  BigInt     @unique
  ballot                       ballot[]
  proposal_proposalToproposals proposal[]
}

model reveal {
  operation_id      BigInt          @id
  source_id         BigInt
  pk                String          @db.Char(55)
  consumed_milligas Decimal?        @db.Decimal
  fee               BigInt
  nonce             Int?
  operation_alpha   operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses         addresses       @relation(fields: [source_id], references: [address_id])

  @@index([pk], name: "reveal_pkh")
  @@index([source_id], name: "reveal_source")
}

model seed_nonce_revelation {
  operation_id    BigInt          @id
  level           Int
  nonce           String          @db.Char(66)
  operation_alpha operation_alpha @relation(fields: [operation_id], references: [autoid])
}

model snapshot {
  cycle              Int
  level              Int
  delegated_contract delegated_contract[]

  @@id([cycle, level])
}

model tx {
  operation_id                           BigInt          @id
  source_id                              BigInt
  destination_id                         BigInt
  fee                                    BigInt
  amount                                 BigInt
  parameters                             String?
  storage                                Json?
  consumed_milligas                      Decimal         @db.Decimal
  storage_size                           Decimal         @db.Decimal
  paid_storage_size_diff                 Decimal         @db.Decimal
  entrypoint                             String?
  nonce                                  Int?
  addresses_addressesTotx_destination_id addresses       @relation("addressesTotx_destination_id", fields: [destination_id], references: [address_id])
  operation_alpha                        operation_alpha @relation(fields: [operation_id], references: [autoid])
  addresses_addressesTotx_source_id      addresses       @relation("addressesTotx_source_id", fields: [source_id], references: [address_id])

  @@index([destination_id], name: "tx_destination")
  @@index([source_id], name: "tx_source")
}
